run:
	docker-compose up

build:
	docker-compose build

build_and_run: build run

black:
	black --exclude .venv .
