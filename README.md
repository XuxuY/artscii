# Projet Artscii
Le projet Artscii est une application web permettant à n'importe qui d'offrir un service de convertion d'image en [ascii-art](https://en.wikipedia.org/wiki/ASCII_art) en ligne facilement.  
Il dispose de plusieurs fonctionnalités et est paramétrisable aisément.

## Membres du projet :
* [Yannick](https://gitlab.com/XuxuY) = Yannick XU
* [Intestinal](https://gitlab.com/Intestinal) = Valentin SCANU
* [Baptiste](https://gitlab.com/bvasseur77) = Baptiste VASSEUR

## Choix du workflow Git : Basic Flow

## Technologies :
* [Python 3.7](https://www.python.org/)
* [Flask 1.1.1](https://palletsprojects.com/p/flask/)

## Explication du choix des technologies :
Le choix de Python couplé avec Flask a été décidé en raison de la taille réduite de l'application ainsi que des connaissances poussées en Python des membres de l'équipe.

## Makefile :
* `make run` : démarre les services.
* `make build` : build les images [docker](https://www.docker.com/get-started).
* `make build_and_run` : build puis démarre les services.
* `make black` : exécute le linter python [black](https://github.com/python/black).

## Installation :
1. Clonez la [source](https://gitlab.com/XuxuY/artscii) avec [git](https://git-scm.com/):
    ```
    $ git clone https://gitlab.com/XuxuY/artscii
    $ cd artscii
    ```
    
2. Lancez l'application avec la commande suivante :
    ```
    $ make build_and_run
    ```

3. Accèdez à l'interface en local via l'url :
    [http://127.0.0.1:5000/](http://127.0.0.1:5000/)

## Fonctionnalités de l'application
Outre la fonction principale de conversion d'une image en ascii-art, il est possible de :
* Modifier les dimensions du résultat
* Choisir l'algorithme de production de l'ascii-art
* Personnaliser les caractères utilisés pour générer le résultat

L'application dispose également d'une fonction sociale étant composé d'un tableau recensant les différentes personnalisation des caractères en entrés et leur nombre d'usage par les utilisateurs .

## Contribuer
Il est vivement encouragé pour ceux qui le désire à contribuer au projet en remontant les bugs via l'[interface de soumission de bug](https://gitlab.com/XuxuY/artscii/issues/new).

## Versions
* 1.0 : Première version d'Artscii. Elle permet pour le moment de simplement convertir une image en ascii-art.
* 1.1 : Dockerisation de l'application.
* 1.2 : Ajout des paramètres de hauteur et de largeur pour l'ascii-art.
* 1.3 : Ajout des choix de l'algorithme de production de l'ascii-art.
* 1.4 : Ajout de la possibilité pour l'utilisateur d'utiliser un ramp personnalisé.
* 2.0 : Ajout de la fonctionnalité permettant de suivre en direct les utilisations de ramp personnalisés par les utilisateurs.
