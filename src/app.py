import os

from flask import Flask, send_file, request, redirect
from ascii import draw_ascii
from utils import get_ramps, update_or_insert

app = Flask(__name__)


@app.route("/")
def home():
    ramps = get_ramps()

    if not ramps:
        with open(os.path.join("html", "home.html")) as f:
            html = f.read().format("")
    else:
        with open(os.path.join("html", "home.html")) as f, open(
            os.path.join("html", "scrollable.html")
        ) as f2:
            html = f.read().format(
                f2.read().format(
                    "</li><li>".join(
                        "|{1}| <span>(utilisé {0} fois)</span>".format(*x)
                        for x in ramps
                    )
                )
            )
    return html


@app.route("/draw", methods=["POST"])
def draw():
    file = request.files["upload"]
    color_method = request.form["color_method"]

    try:
        width = int(request.form["width"])
        height = int(request.form["height"])
    except (ValueError, TypeError):
        width = None
        height = None

    if request.form["ramp"]:
        ramp = request.form["ramp"]
    else:
        ramp = None

    try:
        ascii_drawing = draw_ascii(file, color_method, width, height, ramp)
    except OSError:
        return redirect("/")

    if ramp:
        update_or_insert(ramp)

    with open(os.path.join("html", "draw.html")) as f:
        html = f.read().format(ascii_drawing)
    return html


@app.route("/css.css")
def css():
    return send_file(os.path.join("css", "css.css"))
