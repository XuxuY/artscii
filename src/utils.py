import psycopg2


def get_ramps():
    with psycopg2.connect(
        host="db", dbname="ascii", user="ascii", password="password"
    ) as conn:
        with conn.cursor() as curs:
            curs.execute("SELECT * FROM ramp ORDER BY value")
            ramps = curs.fetchall()
            return ramps


def update_or_insert(value):
    try:
        with psycopg2.connect(
            host="db", dbname="ascii", user="ascii", password="password"
        ) as conn:
            with conn.cursor() as curs:
                curs.execute("INSERT INTO ramp VALUES (1, %s)", (value,))
                conn.commit()

    except psycopg2.errors.UniqueViolation:
        with psycopg2.connect(
            host="db", dbname="ascii", user="ascii", password="password"
        ) as conn:
            with conn.cursor() as curs:
                curs.execute(
                    "UPDATE ramp SET count = count + 1 WHERE value = %s", (value,)
                )
                conn.commit()
