from image import get_pixels

RAMP = "@%#*+=-:. "


def get_char(value, ramp):
    ramp = ramp or RAMP
    return ramp[round(value / (255 / (len(ramp) - 1)))]


def to_grayscale(r, g, b):
    return r * 0.21 + g * 0.72 + b * 0.07


def to_color_mean(r, g, b):
    return (r + g + b) // 3


def to_inverted_color_mean(r, g, b):
    return ((255 - r) + (255 - g) + (255 - b)) // 3


def draw_ascii(path, color_method, width, height, ramp):
    pixels = get_pixels(path, width, height)

    ascii_art = []

    for row in pixels:
        for rgb in row:

            if color_method == "grayscale":
                value = to_grayscale(*rgb)
            elif color_method == "color_mean":
                value = to_color_mean(*rgb)
            else:
                value = to_inverted_color_mean(*rgb)

            ascii_art.append(get_char(value, ramp))
        ascii_art.append("\n")

    return "".join(ascii_art)
