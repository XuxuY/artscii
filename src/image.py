from PIL import Image


def resized_image(image, width, height):
    return image.resize((width, height), Image.ANTIALIAS)


def get_pixels(path, width, height):
    if width and height:
        im = resized_image(Image.open(path), width, height)
    else:
        im = Image.open(path)

    pixels = tuple(im.convert("RGB").getdata())
    width, height = im.size

    return [pixels[i * width : (i + 1) * width] for i in range(height)]
